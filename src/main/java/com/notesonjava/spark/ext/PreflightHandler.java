package com.notesonjava.spark.ext;

import spark.Request;
import spark.Response;
import spark.Route;

public class PreflightHandler implements Route {
	
	private static final String EMPTY_BODY = "";
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		response.status(200);
		return EMPTY_BODY;
	}
}
