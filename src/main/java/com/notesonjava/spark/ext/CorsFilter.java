package com.notesonjava.spark.ext;


import java.util.Arrays;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import spark.Filter;
import spark.Request;
import spark.Response;

@Slf4j
public class CorsFilter implements Filter{

	private List<String> authorizedUrl = Arrays.asList( "https://chamusca-chapina.firebaseapp.com", "http://localhost:4200" );

	@Override
	public void handle(Request req, Response res) throws Exception {
		String origin = req.headers("Origin");
		if(authorizedUrl.contains(origin)) {
			res.header("Access-Control-Allow-Origin", origin);	
		}
		res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
		res.header("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
		res.header("Access-Control-Request-Method", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
		res.header("Access-Control-Allow-Credentials", "true");
		
		log.debug("*** Response header");
		res.raw().getHeaderNames().forEach(name ->  log.debug(name +", "+ res.raw().getHeader(name)));
		log.debug("*** ");

	}
}
